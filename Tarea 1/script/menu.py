#! /usr/bin/python3
# coding=utf-8

from os import listdir
import os, sys

def make_menu(rute,filter =""):
    menu  = "\n    Escoja su opción :\n\n"
    menu += "\t0] salir\n"
    try:    
        options = listdir(rute)
        for i in range(len(options)):
            menu = menu + "\t" +str(i+1) + "] " + options[i] + '\n'
        return [menu, options]
    except:
        print("   [ERROR] No existe el path " + rute)
        sys.exit()
    
def choose_option(rute, filter=""):
    str_menu, options = make_menu(rute,filter)
    if(len(options) != 0):
        print (str_menu)
        choosen = input()
        try:
            choosen = int(choosen)
        except:
            return ""
        if (choosen == 0 ):
            sys.exit()
            return ""
        elif (choosen > len(options)):
            print ("  [ERROR] Opción inválida")
            sys.exit()
        else:
            return options[int(choosen)-1]    
        return ""
    else:
        print("   [ERROR] No hay opciones disponibles")
        return ""

if __name__ == "__main__":
    option  = "./bin/" + choose_option("./")
    option  = option.replace(".py",".out")
    command = "make " + option
    os.system(command)

