#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, re                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                

def create_dir(path):
    try:
        os.stat(path)
    except:
        os.mkdir(path)

def create_module(r_string, temp_path, out_path):
    p_name   = re.compile(r".*NAME.*")
    try:
        mod_file  = open(out_path, "w")
        temp_file = open(temp_path, "r")
        for line in temp_file:
            if p_name.match(line):
                line = line.replace("NAME",r_string.upper())
            mod_file.write(line)
        temp_file.close()
        mod_file.close()
    except:
        print("  [ERROR] En la lectura y escritura") 
    pass
        
if __name__ == "__main__":
    # Creación de directorio include
    create_dir("./include")

    mod_name   = input("Nombre del header: ")
    mod_name   = mod_name.lower()
    
    out_path  = "./include/" + mod_name + ".h"
    # Se comprueba que el modulo no exista
    if os.path.exists(out_path):
        print("  [ERROR] Ya existe un header con ese nombre")
    else:
        # Creación del módulo
        temp_path = "./template/header"
        create_module(mod_name,temp_path,out_path)