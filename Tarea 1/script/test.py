#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import menu, re, os, sys

def scan_test(name):
    dependecy_files = []
    test_file = open("./test/"+name+".c","r")
    for line in test_file:
        m = re.match(r"\#include\s+\"(.*)\"",line) 
        if(m):
            dependecy_files.append(m.group(1).replace(".h",""))
    return dependecy_files        

def dep_compile(dependencies):
    for dep in dependencies:
        os.system("make ./bin/"+dep+".o")

def build_test(dependencies, test_name):
    command = "gcc -g -DTEST --debug -I ./include -Wall -pedantic "
    for dep in dependencies:
        command += "./bin/" + dep + ".o "
    command += "./test/"+ test_name + ".c -o ./test.out -lm"
    os.system(command)

def run_test(test_name, test_memory = False):
    dep = scan_test(test_name)
    dep_compile(dep)
    build_test(dep, test_name)
    print("############################################")
    print("##         Ejecutando el test ...        ###")
    print("############################################")
    if(test_memory):
        os.system("valgrind --leak-check=yes cat ./data/prueba.txt|./test.out")
    else:
        os.system("cat ./data/prueba.txt|./test.out")
    print("Listo:::::::::::::::::::::::::::::::::::::::")

if __name__ == "__main__":
    option = menu.choose_option("./test")
    option  = option.replace(".c","")

    if(sys.argv[1] == "t"):
        run_test(option,False)
    elif(sys.argv[1] == "m"):
        run_test(option,True)
