#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import listdir
import os, re     

def create_dir(path):
    try:
        os.stat(path)
    except:
        os.mkdir(path)


def scan_header(header_name):
    header_path = "./include/" + header_name + ".h"
    functions = []
    try:
        header_file = open(header_path, "r")
        for line in header_file:
            m = re.match(r"(.*\s+.*\(.*\))(\s+)?;",line)
            if(m):
                functions.append(m.group(1))
    except:
        print("  [ERROR] En la lectura del header") 
    return functions

def scan_src(src_name):
    src_path = "./src/" + src_name + ".c"
    functions = []
    try:
        src_file = open(src_path, "r")
        for line in src_file:
            m = re.match(r"(.*\s+.*\(.*,.*\))(\s+)?({)?",line)
            if(m):
                functions.append(m.group(1))
    except:
        print("  [ERROR] En la lectura del código") 
    return functions

def add_func(src_name, functions, add=True):
    if(add):
        src_file = open("./src/"+src_name+".c","w")
        src_file.write("#include \""+ src_name+".h\"\n\n")
    else:
        src_file = open("./src/"+src_name+".c","a")
    for function in functions:
        src_file.write(function+"{\n\n}\n\n")
        

def auto_gen(headers_names):
    for name in headers_names:
        h_functions = scan_header(name)
        if os.path.exists("./src/"+name+".c"):
            functions    = []
            src_funcions = scan_src(name)
            for i in range(len(h_functions)):
                add_function = True
                for j in range(len(src_funcions)):
                    if(h_functions[i].replace(" ","") == src_funcions[j].replace(" ","")):
                        add_function = False
                if(add_function):
                    functions.append(h_functions[i])
            add_func(name, functions, False)
        else:
            add_func(name, h_functions)
    
if __name__ == "__main__":
    headers = listdir("./include")
    headers_names = []
    for i in range(len(headers)):
        headers_names.append(headers[i].replace(".h",""))
    auto_gen(headers_names)
