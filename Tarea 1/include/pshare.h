/**
 * @file pshare.h
 * @author Mariela Hernández Chacón
 * @brief Logic for Pshare prediction
 * @version 0.1
 * @date 2019-09-18
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _PSHARE_H_
#define _PSHARE_H_

// INCLUDES
#include "library.h"
#include <stdlib.h>
#include <stdio.h>

// STRUCTS 

// FUNCIONS
/**
 * @brief Creates PHT
 * 
 * @param s Number of bits for indexing
 * @return unsigned int* returns PHT
 */
unsigned int* create_PHT(int s);

/**
 * @brief Get the index for PHT
 * 
 * @param s Number of bits for indexing
 * @param PC Program counter
 * @return int Returns index
 */
int get_index_PHT(int s, unsigned int PC);

/**
 * @brief Get the index for pshare 
 * 
 * @param s Number of bits for indexing
 * @param PC Program counter
 * @param reg Register
 * @return int Returns index
 */
int get_index_pshare(int s, unsigned int PC, unsigned int reg);

/**
 * @brief Pshare predictor logic
 * 
 * @param BHT BHT table
 * @param PHT PHT table
 * @param s Number of bits for indexing
 * @param PC Program counter
 * @param outcome General outcome
 * @param res Results
 * @param ph Global register size
 * @return prediction* 
 */
prediction* pshare(char* BHT, unsigned int* PHT, int s, unsigned int PC, char* outcome, results* res, int ph);

/**
 * @brief Free memory
 * 
 * @param PHT PHT table
 */
void free_PHT(unsigned int* PHT);

#endif /* _PSHARE_H_ */
