/**
 * @file gshare.h
 * @author Mariela Hernández Chacón
 * @brief Logic for Gshare prediction
 * @version 0.1
 * @date 2019-09-18
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _GSHARE_H_
#define _GSHARE_H_

// INCLUDES
#include "library.h"
#include <stdlib.h>
#include <stdio.h>

// STRUCTS 

// FUNCIONS
/**
 * @brief Get the index for gshare
 * 
 * @param s Number of bits for indexing
 * @param PC Program counter
 * @param reg Register
 * @return int Returns index
 */
int get_index_gshare(int s, unsigned int PC, unsigned int reg);

/**
 * @brief Gshare predictor logic
 * 
 * @param BHT BHT table
 * @param reg Register
 * @param s Number of bits for indexing
 * @param PC Program counter
 * @param outcome General outcome
 * @param res Results struct
 * @return prediction* Returns prediction struct
 */
prediction* gshare(char* BHT, unsigned int reg, int s, unsigned int PC, char* outcome, results* res);

#endif /* _GSHARE_H_ */
