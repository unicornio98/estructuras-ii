/**
 * @file bimodal.h
 * @author Mariela Hernández Chacón
 * @brief Logic for bimodal prediction
 * @version 0.1
 * @date 2019-09-18
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _BIMODAL_H_
#define _BIMODAL_H_

#define PREDICTION_SIZE 25 //cambiar por 2^s
// INCLUDES
#include "library.h"
#include "write_file.h"
#include "gshare.h"
#include <stdlib.h>
#include <stdio.h>

// STRUCTS 

// FUNCIONS 
/**
 * @brief Get the index for bimodal
 * 
 * @param s Number of bits for indexing
 * @param PC Program counter
 * @return int Returns index
 */
int get_index_bimodal(int s, unsigned int PC);

/**
 * @brief Bimodal predictor logic
 * 
 * @param BHT BHT table
 * @param s Number of bits for indexing
 * @param PC Program counter
 * @param outcome General outcome
 * @param res Results struct
 * @return prediction* Returns prediction struct
 */
prediction* bimodal(char* BHT, int s, unsigned int PC, char* outcome, results* res);

#endif /* _BIMODAL_H_ */
