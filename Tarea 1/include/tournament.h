/**
 * @file tournament.h
 * @author Mariela Hernández Chacón
 * @brief Logic for tournament prediction
 * @version 0.1
 * @date 2019-09-18
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _TOURNAMENT_H_
#define _TOURNAMENT_H_

// INCLUDES
#include "library.h"
#include "pshare.h"
#include "gshare.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

// STRUCTS 
/**
 * @brief Struct for storing results of prediction
 * 
 */
typedef struct
{
    char* predictor;
    char* prediction;
    char* value;
}metaPrediction;

// FUNCIONS
/**
 * @brief Create a meta prediction object
 * 
 * @return metaPrediction* Returns metaPrediction* structure
 */
metaPrediction* create_meta_prediction();

/**
 * @brief Create a BHT meta object
 * 
 * @param s Number of bits for indexing
 * @param res Results' structure
 * @return char* Returns BHT
 */
char* create_BHT_meta(int s, results* res);

/**
 * @brief Get the index for BHT table
 * 
 * @param s Number of bits for indexing
 * @param PC Program counter
 * @return int Returns index
 */
int get_index_meta(int s, unsigned int PC);

/**
 * @brief Tournament preditor logic
 * 
 * @param s Number of bits for indexing
 * @param PC Program counter
 * @param outcome General outcome
 * @param reg Register 
 * @param ph Global register size
 * @param gh Register size of PHT
 * @param res Results
 * @param res_meta Results from tournament
 * @param BHT_pshare BHT from Pshare
 * @param BHT_gshare BHT from Gshare
 * @param BHT_meta BHT from Tournament
 * @param PHT PHT from Pshare 
 * @return metaPrediction* 
 */
metaPrediction* tournament(int s, unsigned int PC, char* outcome, unsigned int* reg, int ph, int gh, results* res, results* res_meta, char* BHT_pshare, char* BHT_gshare, char* BHT_meta, unsigned int* PHT);

/**
 * @brief Frees memory
 * 
 * @param meta metaPrediction struct
 */
void free_meta_prediction(metaPrediction* meta);

/**
 * @brief Frees memory
 * 
 * @param meta BHT meta table
 */
void free_BHT_meta(char* meta);
#endif /* _TOURNAMENT_H_ */
