/**
 * @file write_file.h
 * @author Mariela Hernández Chacón
 * @brief Writes on corresponding file, according to predictor
 * @version 0.1
 * @date 2019-09-18
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _WRITE_FILE_H_
#define _WRITE_FILE_H_

// INCLUDES
#include <stdio.h>
#include <stdlib.h>

// STRUCTS 
/**
 * @brief Struct for storing general results
 * 
 */
typedef struct
{   
    char* prediction_type;
    int BHT_size;
    int g_size;
    int p_size;
    int correct_taken;
    int incorrect_taken;
    int correct_not_taken;
    int incorrect_not_taken;
}results; 

// FUNCIONS
/**
 * @brief Creates a structure to store main results for printing at the end
 * 
 * @return results* 
 */
results* create_results();

/**
 * @brief Opens or creates a file object and prints the header, according to the type of predictor 
 * 
 * @param filename 
 * @param bp Which predictor is using
 */
void create_file(char* filename, int bp);

/**
 * @brief Writes a line in corresponding predictor file
 * 
 * @param filename 
 * @param PC Program counter
 * @param outcome Outcome of prediction
 * @param prediction Prediction value, 'T' or 'N'
 * @param value Prediction value, 'correct' or 'incorrect'
 * @param bp Which predictor is using
 * @param predictor Name of predictor
 */
void write_line(char* filename, unsigned int PC, char* outcome, char* prediction, char* value, int bp, char* predictor);

/**
 * @brief Writes a line in the corresponding file
 * 
 * @param prediction_type Prediction type
 * @param BHT_size Size of BHT 
 * @param g_size Global register size
 * @param p_size Register size of PHT
 * @param num_branch Number of branches
 * @param correct_taken Correct taken
 * @param incorrect_taken Incorrect not taken
 * @param correct_not_taken Correct not taken
 * @param incorrect_not_taken Incorrect not taken
 */
void print_on_screen(char* prediction_type, int BHT_size, int g_size,  int p_size, int num_branch, int correct_taken, int incorrect_taken, int correct_not_taken, int incorrect_not_taken);

/**
 * @brief Free memory
 * 
 * @param res Result struct
 */
void free_results(results* res);

#endif /* _WRITE_FILE_H_ */