/**
 * @file parse_file.h
 * @author Mariela Hernández Chacón
 * @brief Parses a line of the trace
 * @version 0.1
 * @date 2019-09-18
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _PARSE_FILE_H_
#define _PARSE_FILE_H_

#define BUFFER_SIZE 25
// INCLUDES
#include "tournament.h"
#include "bimodal.h"
#include "write_file.h"
#include "gshare.h"
#include "pshare.h"
#include "library.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// STRUCTS 
/**
 * @brief Struct for each trace
 * 
 */
typedef struct
{
    unsigned int instr;
    char* outcome;
}dataTrace;

// FUNCIONS
/**
 * @brief Create a data trace object
 * 
 * @return dataTrace* 
 */
dataTrace* create_data_trace();

/**
 * @brief Main function: chooses between preditors
 * 
 * @param file File object
 * @param s Number of bits for indexing
 * @param bp Type of predictor
 * @param gh Register size of PHT
 * @param ph Global register size
 * @param o Write on file
 */
void parse_file(FILE* file, int s, int bp, int gh, int ph, int o);
dataTrace* parse_line(char* line);

/**
 * @brief Deletes data trace
 * 
 * @param data dataTrace
 */
void delete_data_trace(dataTrace* data);

#endif /* _PARSE_FILE_H_ */
