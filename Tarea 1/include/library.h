/**
 * @file library.h
 * @author Mariela Hernández Chacón
 * @brief Shared functions for all predictors
 * @version 0.1
 * @date 2019-09-18
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef _LIBRAY_H_
#define _LIBRAY_H_
#include "write_file.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// STRUCTS 
/**
 * @brief Prediction struct
 * 
 */
typedef struct
{
    char* prediction;
    char* value;
}prediction;

// FUNCIONS
/**
 * @brief Creates a prediction object
 * 
 * @return prediction* 
 */
prediction* create_prediction();

/**
 * @brief Creates a BHT
 * 
 * @param s Number of bits for indexing
 * @param res Results struct
 * @return char* Returns BHT
 */
char* create_BHT(int s, results* res);

/**
 * @brief Create a register object
 * 
 * @param res Results struct
 * @param g_size Register size of PHT
 * @param p_size Global register size
 * @param bp Type of predictor
 * @return unsigned int Returns register
 */
unsigned int create_register(results* res, int g_size, int p_size, int bp);

/**
 * @brief Updates register for Gshare
 * 
 * @param reg Register
 * @param outcome General outcome
 * @param gh Register size of PHT
 * @return unsigned int Returns new register
 */
unsigned int history_register(unsigned int reg, char* outcome, int gh);

/**
 * @brief Updates register for Pshare
 * 
 * @param reg Register
 * @param outcome General outcome
 * @param ph Global register size
 * @return unsigned int Returns register
 */
unsigned int update_register_PHT(unsigned int reg, char* outcome, int ph);

/**
 * @brief Two bit counter predictor
 * 
 * @param BHT BHT table
 * @param index Index for table
 * @param outcome General outcome
 * @param res Results struct
 * @return prediction* Returns prediction* object
 */
prediction* two_bit_counter_predictor(char* BHT, int index, char* outcome, results* res);

/**
 * @brief Frees memory
 * 
 * @param pred Prediction object
 */
void free_prediction(prediction* pred);

/**
 * @brief Frees memory
 * 
 * @param BHT BHT table
 */
void free_BHT(char* BHT);

#endif /* _LIBRAY_H_ */
