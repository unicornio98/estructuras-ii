#include "library.h"

prediction* create_prediction(){
    prediction* pred = malloc(sizeof(prediction));
    pred->prediction = malloc(sizeof(char));
    pred->value = malloc(25*sizeof(char));
    return pred;
}

char* create_BHT(int s, results* res){
    // Size of BHT
    double BHT_SIZE = pow(2, (double)s);

    // Reserve space for BHT
    char* BHT = malloc((int)BHT_SIZE*sizeof(char));

    // SN initial state for all counter in BHT
    for (int i = 0; i < (int)BHT_SIZE; i++)
    {
        BHT[i] = 'N';
    }

    // printf("Initial BHT: %s \n", BHT);
    // printf("Size of BHT: %d\n\n", (int)sizeof(BHT));

    // Store results
    res->BHT_size = (int)BHT_SIZE;
    return BHT;
}

unsigned int create_register(results* res, int g_size, int p_size, int bp){
    if (bp == 0)
    {
        res->g_size = 0;
        res->p_size = 0;
    }
    else if (bp == 1) // PShare
    {
        res->g_size = 0;
        res->p_size = p_size; 
    }
    else if (bp == 2) // GShare
    {
        res->g_size = g_size;
        res->p_size = 0;
    }
    else if (bp == 3) // Tournament
    {
        res->g_size = g_size;
        res->p_size = p_size;
    }

    // Set history reg as N
    unsigned int reg = 0; 

    return reg;
}

unsigned int history_register(unsigned int reg, char* outcome, int gh){
    unsigned int new_reg = reg << 1;

    if (*outcome == 'T')
    {
        new_reg++;
    }

    // Apply mask to get reg of gh bits
    unsigned int max_value = 0xffffffff;
    unsigned int mask = max_value >> (32 - gh);
    unsigned int gh_reg = mask & new_reg;

    return gh_reg; 
}

unsigned int update_register_PHT(unsigned int reg, char* outcome, int ph){
    unsigned int new_reg = reg << 1;

    // Add one if TAKEN
    if (*outcome == 'T')
    {
        new_reg++;
    }

    // Apply mask to get ph bits
    unsigned int max_value = 0xffffffff;
    unsigned int mask = max_value >> (32 - ph);
    unsigned int ph_reg = mask & new_reg;

    return ph_reg; 
}

prediction* two_bit_counter_predictor(char* BHT, int index, char* outcome, results* res){
    prediction* pred = create_prediction();

    // Define actual state
    char state = BHT[index];

    if (*outcome == 'T')
    {
        if (state == 'N') // Predicts NT
        {
            BHT[index] = 'n'; 
            res->incorrect_not_taken++;
            pred->prediction = "N";
            pred->value = "incorrect";
        }
        else if (state == 'n') // Predicts NT
        {
            BHT[index] = 't';
            res->incorrect_not_taken++;
            pred->prediction = "N";
            pred->value = "incorrect";
        }
        else if (state == 't') // Predicts T
        {
            BHT[index] = 'T';
            res->correct_taken++;
            pred->prediction = "T";
            pred->value = "correct";
        }
        else if (state == 'T') // Predicts T
        {
            BHT[index] = 'T';
            res->correct_taken++;
            pred->prediction = "T";
            pred->value = "correct";
        }
    }
    else if (*outcome == 'N')
    {
        if (state == 'T') // Predicts T
        {
            BHT[index] = 't'; 
            res->incorrect_taken++;
            pred->prediction = "T";
            pred->value = "incorrect";
        }
        else if (state == 't') // Predicts T
        {
            BHT[index] = 'n';
            res->incorrect_taken++;
            pred->prediction = "T";
            pred->value = "incorrect";
        }
        else if (state == 'n') // Predicts NT
        {
            BHT[index] = 'N';
            res->correct_not_taken++;
            pred->prediction = "N";
            pred->value = "correct";
        }
        else if (state == 'N') // Predicts NT
        {
            BHT[index] = 'N';
            res->correct_not_taken++;
            pred->prediction = "N";
            pred->value = "correct";
        }
    }
    // printf("Prediction: %s, with value: %s\n", pred->prediction, pred->value);
    // printf("New BHT: %s \n\n", BHT);

    return pred;
}

void free_prediction(prediction* pred){
    free(pred->prediction);
    free(pred->value);
    free(pred);
}

void free_BHT(char* BHT){
    free(BHT);
}

