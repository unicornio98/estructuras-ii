#include "tournament.h"

metaPrediction* create_meta_prediction(){
    metaPrediction* meta = malloc(sizeof(metaPrediction));
    meta->predictor = malloc(sizeof(char));
    meta->prediction = malloc(sizeof(char));
    meta->value = malloc(25*sizeof(char));
    return meta;
}

char* create_BHT_meta(int s, results* res){
    // Size of BHT
    double BHT_SIZE = pow(2, (double)s);

    // Reserve space for BHT
    char* BHT = malloc((int)BHT_SIZE*sizeof(char));

    // SN initial state for all counter in BHT
    for (int i = 0; i < (int)BHT_SIZE; i++)
    {
        BHT[i] = 'P';
    }

    // printf("Initial BHT metapredictor: %s \n", BHT);
    // printf("Size of BHT metapredictor: %d\n\n", (int)sizeof(BHT));

    // Store results
    res->BHT_size = (int)BHT_SIZE;
    return BHT;
}

int get_index_meta(int s, unsigned int PC){
    unsigned int max_value = 0xffffffff;

    // Shifting right
    unsigned int mask = max_value >> (32 - s);

    // And between mask and PC in order to find the entry index
    unsigned int index = mask & PC;
    // printf("Index meta: %u\n", index);

    return (int)index;
}

metaPrediction* tournament(int s, unsigned int PC, char* outcome, unsigned int* reg, int ph, int gh, results* res, results* res_meta, char* BHT_pshare, char* BHT_gshare, char* BHT_meta, unsigned int* PHT){
    // Create meta prediction structure
    metaPrediction* pred_meta = create_meta_prediction(); 

    // Get index
    int index = get_index_meta(s, PC);

    // Execute both PShare and Gshare
    prediction* pred_pshare = pshare(BHT_pshare, PHT, s, PC, outcome, res, ph);
    prediction* pred_gshare = gshare(BHT_gshare, *reg, s, PC, outcome, res);
    // Update history register
    *reg = history_register(*reg, outcome, gh);

    // Get state
    char state = BHT_meta[index];

    // Update BHT meta
    if (strcmp(pred_pshare->value, pred_gshare->value) == 0) // If both are equal
    {
        if (state == 'p' || state == 'P')
        {
            *pred_meta->predictor = 'P'; 
        }
        else if (state == 'g' || state == 'G')
        {
            *pred_meta->predictor = 'G';
        }
        // Could be PShare or GShare, both are equal
        pred_meta->prediction =  pred_pshare->prediction; 
        pred_meta->value = pred_pshare->value;

        // Check if correct or incorrect T or N
        if (strcmp(pred_pshare->value, "correct") == 0)
        {
            if (*pred_pshare->prediction == 'T')
            {
                res_meta->correct_taken++;
            }
            else
            {
                res_meta->correct_not_taken++;
            }
        }
        else
        {
            if (*pred_pshare->prediction == 'T')
            {
                res_meta->incorrect_taken++;
            }
            else
            {
                res_meta->incorrect_not_taken++;
            }
        }
        // Print
        // printf("Both correct/incorrect: %s \n", pred_pshare->value);
    }
    else if (strcmp(pred_gshare->value, "incorrect") == 0 && strcmp(pred_pshare->value, "correct") == 0)// If PShare is correct
    {
        if (state == 'P') // Takes Pshare predictor
        {
            *pred_meta->predictor = 'P';
            pred_meta->prediction =  pred_pshare->prediction;
            pred_meta->value = pred_pshare->value;

            if (*pred_pshare->prediction == 'N')
            {
                res_meta->correct_not_taken++;
            }
            else
            {
                res_meta->correct_taken++;
            }
            BHT_meta[index] = 'P'; // Next state
        }
        else if (state == 'p') // Takes Pshare predictor
        {
            *pred_meta->predictor = 'P';
            pred_meta->prediction =  pred_pshare->prediction;
            pred_meta->value = pred_pshare->value;

            if (*pred_pshare->prediction == 'N')
            {
                res_meta->correct_not_taken++;
            }
            else
            {
                res_meta->correct_taken++;
            }
            BHT_meta[index] = 'P'; // Next state
        }
        else if (state == 'g') // Takes Gshare predictor
        {           
            *pred_meta->predictor = 'G';
            pred_meta->prediction =  pred_gshare->prediction;
            pred_meta->value = pred_gshare->value;

            if (*pred_gshare->prediction == 'N')
            {
                res_meta->incorrect_not_taken++;
            }
            else
            {
                res_meta->incorrect_taken++;
            }
            BHT_meta[index] = 'p'; // Next state
        }
        else if (state == 'G') // Takes Gshare predictor
        {          
            *pred_meta->predictor = 'G';
            pred_meta->prediction =  pred_gshare->prediction;
            pred_meta->value = pred_gshare->value;

            if (*pred_gshare->prediction == 'N')
            {
                res_meta->incorrect_not_taken++;
            }
            else
            {
                res_meta->incorrect_taken++;
            }
            BHT_meta[index] = 'g'; // Next state
        }
        // Print
        // printf("Pshare correct (%s) and Gshare incorrect (%s): \n", pred_pshare->value, pred_gshare->value); 
    }
    else if (strcmp(pred_gshare->value, "correct") == 0 && strcmp(pred_pshare->value, "incorrect") == 0) // If Gshare is correct
    {
        if (state == 'P') // Takes Pshare predictor
        {         
            *pred_meta->predictor = 'P';
            pred_meta->prediction =  pred_pshare->prediction;
            pred_meta->value = pred_pshare->value;

            if (*pred_pshare->prediction == 'N')
            {
                res_meta->incorrect_not_taken++;
            }
            else
            {
                res_meta->incorrect_taken++;
            }
            BHT_meta[index] = 'p'; // Next state
        }
        else if (state == 'p') // Takes Pshare predictor
        {            
            *pred_meta->predictor = 'P';
            pred_meta->prediction =  pred_pshare->prediction;
            pred_meta->value = pred_pshare->value;

            if (*pred_pshare->prediction == 'N')
            {
                res_meta->incorrect_not_taken++;
            }
            else
            {
                res_meta->incorrect_taken++;
            }
            BHT_meta[index] = 'g'; // Next state
        }
        else if (state == 'g') // Takes Gshare predictor
        {            
            *pred_meta->predictor = 'G';
            pred_meta->prediction =  pred_gshare->prediction;
            pred_meta->value = pred_gshare->value;

            if (*pred_gshare->prediction == 'N')
            {
                res_meta->correct_not_taken++;
            }
            else
            {
                res_meta->correct_taken++;
            }
            BHT_meta[index] = 'G'; // Next state
        }
        else if (state == 'G') // Takes Gshare predictor
        {
            *pred_meta->predictor = 'G';
            pred_meta->prediction =  pred_gshare->prediction;
            pred_meta->value = pred_gshare->value;

            if (*pred_gshare->prediction == 'N')
            {
                res_meta->correct_not_taken++;
            }
            else
            {
                res_meta->correct_taken++;
            }
            BHT_meta[index] = 'G'; // Next state
        } 
        // Print
        // printf("Pshare incorrect (%s) y Gshare correct (%s): \n", pred_pshare->value, pred_gshare->value);
    }

    // printf("Predictor: %s, Prediction: %s, with value: %s\n", pred_meta->predictor, pred_meta->prediction, pred_meta->value);
    // printf("New BHT meta: %s \n\n", BHT_meta);

    return pred_meta;
}

void free_meta_prediction(metaPrediction* meta){
    free(meta->predictor);
    free(meta->prediction);
    free(meta->value);
    free(meta);
}

void free_BHT_meta(char* meta){
    free(meta);
}