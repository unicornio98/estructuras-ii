#include "pshare.h"

unsigned int* create_PHT(int s){
    // Size of PHT
    double PHT_SIZE = pow(2, (double)s);

    // Reserve space for PHT
    unsigned int *PHT = malloc((int)PHT_SIZE*sizeof(unsigned int));

    // SN initial state for all counter in BHT
    for (int i = 0; i < (int)PHT_SIZE; i++)
    {
        PHT[i] = 0;
    }

    // printf("Initial PHT: %u \n", *PHT);
    // printf("Size of PHT: %d\n\n", (int)sizeof(PHT));

    return PHT;
}

int get_index_PHT(int s, unsigned int PC){
    unsigned int max_value = 0xffffffff;

    // Shifting right
    unsigned int mask = max_value >> (32 - s);

    // And between mask and PC in order to find the entry index
    unsigned int index = mask & PC;
    // printf("Index PHT: %u\n", index);

    return (int)index;
}

int get_index_pshare(int s, unsigned int PC, unsigned int reg){
    unsigned int max_value = 0xffffffff;

    // Shifting right
    unsigned int mask = max_value >> (32 - s);

    // XOR between PC and register
    unsigned int xor_pshare = PC ^ reg;

    // And between mask and XOR in order to find the entry index
    unsigned int index = mask & xor_pshare;

    // printf("Index PShare: %u\n", index);

    return (int)index;
}

prediction* pshare(char* BHT, unsigned int* PHT, int s, unsigned int PC, char* outcome, results* res, int ph){
    // Gets history register from PHT
    int index_PHT = get_index_PHT(s, PC);
    unsigned int reg = PHT[index_PHT];

    int index = get_index_pshare(s, PC, reg);

    prediction* psharePrediction = two_bit_counter_predictor(BHT, index, outcome, res);

    // Update register
    PHT[index_PHT] = update_register_PHT(reg, outcome, ph);

    return psharePrediction;
}

void free_PHT(unsigned int* PHT){
    free(PHT);
}