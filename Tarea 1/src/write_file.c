#include "write_file.h"

results* create_results(){
    results* res = malloc(sizeof(results));
    res->prediction_type = malloc(25*sizeof(char));
    res->BHT_size = 0;
    res->g_size = 0;
    res->p_size = 0;
    res->correct_taken = 0;
    res->incorrect_taken = 0;
    res->correct_not_taken = 0;
    res->incorrect_not_taken = 0; 
    return res;
}

void create_file(char* filename, int bp){
    FILE* file_ptr = fopen(filename, "w");

    // Write header
    if (bp == 3)
    {
        fprintf(file_ptr, "PC         | Predictor | Outcome | Prediction | correct/incorrect\n");
    }
    else
    {
        fprintf(file_ptr, "PC         | Outcome | Prediction | correct/incorrect\n");
    }

    fclose(file_ptr);
}

void write_line(char* filename, unsigned int PC, char* outcome, char* prediction, char* value, int bp, char* predictor){
    FILE* file_ptr = fopen(filename, "a");

    // Write line
    if (bp == 3)
    {
        fprintf(file_ptr, "%u | %c         | %s       | %s          | %s\n", PC, *predictor, outcome, prediction, value);
    }
    else
    {
        fprintf(file_ptr, "%u | %s       | %s          | %s\n", PC, outcome, prediction, value);
    }

    fclose(file_ptr);
}

void print_on_screen(char* prediction_type, int BHT_size, int g_size,  int p_size, int num_branch, int correct_taken, int incorrect_taken, int correct_not_taken, int incorrect_not_taken){
    //Percentage of correct predictions
    double correct_rate = ( ((double)correct_taken + (double)correct_not_taken) / (double)num_branch) * 100;

    // Prediction parameters
    printf("-------------------------------------------------------------\n");
    printf("Prediction parameters:\n");
    printf("-------------------------------------------------------------\n");
    printf("Branch prediction type: %s\n", prediction_type);
    printf("BHT size (entries): %d\n", BHT_size);
    printf("Global history register size: %d\n", g_size);
    printf("Private history register size: %d\n", p_size);

    // Simulation results
    printf("-------------------------------------------------------------\n");
    printf("Simulation results:\n");
    printf("-------------------------------------------------------------\n");
    printf("Number of branch: %d\n", num_branch);
    printf("Number of correct prediction of taken branches: %d\n", correct_taken);
    printf("Number of incorrect prediction of taken branches: %d\n", incorrect_taken);
    printf("Correct prediction of not taken branches: %d\n", correct_not_taken);
    printf("Incorrect prediction of not taken branches: %d\n", incorrect_not_taken);
    printf("Percentage of correct predictions: %lf%%\n", correct_rate);
    printf("-------------------------------------------------------------\n");
}

void free_results(results* res){
    free(res->prediction_type);
    free(res);
}


