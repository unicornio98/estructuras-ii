#include "parse_file.h"
#include "write_file.h"
#include "bimodal.h"
#include "gshare.h"
#include "pshare.h"
#include "library.h"
#include "tournament.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


int main(int argc, char const *argv[])
{
 
    unsigned int s, bp, ph, gh, o; 
    for (size_t i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-s") == 0)
        {
            s = atoi(argv[i+1]);
        }
        else if (strcmp(argv[i], "-bp") == 0)
        {
            bp = atoi(argv[i+1]);
        }
        else if (strcmp(argv[i], "-gh") == 0)
        {
            gh = atoi(argv[i+1]);
        }
        else if (strcmp(argv[i], "-ph") == 0)
        {
            ph = atoi(argv[i+1]);
        }
        else if (strcmp(argv[i], "-o") == 0)
        {
            o  = atoi(argv[i+1]);
        }        
        
    }
    // Parse file and execute prediction
    parse_file(stdin, s, bp, gh, ph, o);
    return 0;
}
