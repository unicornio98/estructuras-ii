#include "bimodal.h"

int get_index_bimodal(int s, unsigned int PC){
    unsigned int max_value = 0xffffffff;

    // Shifting right
    unsigned int mask = max_value >> (32 - s);

    // And between mask and PC in order to find the entry index
    unsigned int index = mask & PC;
    // printf("Index: %u\n", index);

    return (int)index;
}

prediction* bimodal(char* BHT, int s, unsigned int PC, char* outcome, results* res){
    // Get index
    int index = get_index_bimodal(s, PC);

    prediction* bimodalPrediction = two_bit_counter_predictor(BHT, index, outcome, res);

    return bimodalPrediction;
}