#include "gshare.h"

int get_index_gshare(int s, unsigned int PC, unsigned int reg){
    unsigned int max_value = 0xffffffff;

    // Shifting right
    unsigned int mask = max_value >> (32 - s);

    // Mask to PC
    unsigned int mask_PC = PC & mask;

    // XOR between PC and register
    unsigned int xor_gshare = mask_PC ^ reg;

    // And between mask and XOR in order to find the entry index
    unsigned int index = mask & xor_gshare;

    // printf("Index GShare: %u\n", index);

    return (int)index;
}

prediction* gshare(char* BHT, unsigned int reg, int s, unsigned int PC, char* outcome, results* res){
    // Get index
    int index = get_index_gshare(s, PC, reg);

    prediction* gsharePrediction = two_bit_counter_predictor(BHT, index, outcome, res);

    return gsharePrediction;
}