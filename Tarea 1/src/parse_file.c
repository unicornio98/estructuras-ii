#include "parse_file.h"

dataTrace* create_data_trace(){
    dataTrace* data = malloc(sizeof(dataTrace));
    data->instr = 0;
    data->outcome = malloc(BUFFER_SIZE*sizeof(char));
    return data;
}

void parse_file(FILE* file, int s, int bp, int gh, int ph, int o){
    char caracteres[100];

 	if (file == NULL){
         exit(1);
    }
    else
    {
        // branch counter
        int num_branch = 0;

        // Store all printable results here
        results* res = create_results();

        // Results for metapredictor
        results* res_meta = create_results();

        // Create new BHT for bimodal
        char* BHT_bimodal = create_BHT(s, res);

        // Create new BHT for pshare
        char* BHT_pshare = create_BHT(s, res);

        // Create new BHT for gshare
        char* BHT_gshare = create_BHT(s, res);

        // Create new BHT for tournament
        char* BHT_meta = create_BHT_meta(s, res_meta);

        // Create new PHT
        unsigned int* PHT = create_PHT(s);

        // Create register
        unsigned int reg = create_register(res, gh, ph, bp);
        
        // CHOOSES PREDICTOR:
        // 0: Bimodal
        // 1: PShare
        // 2: GShare
        // 3: Tournament

        if (bp == 0)
        {
            res->prediction_type = "Bimodal";
        }
        else if (bp == 1)
        {
            res->prediction_type = "PShare";
        }
        else if (bp == 2)
        {
            res->prediction_type = "GShare";
        }
        else if (bp == 3)
        {
            res->prediction_type = "Tournament";
        }

        // Create file if it's necessary
        char* filename = malloc(15*sizeof(char));
        if (o == 1)
        {   
            filename = strcat(strcat(filename, res->prediction_type), ".txt");
            create_file(filename, bp);
        }
         
        // Reads one line of the file
        while (fgets(caracteres, 100, file) != NULL){
            // printf("Num branch: %d \n", num_branch);
            
            num_branch++;
            // Separates outcome and PC
            dataTrace* data = parse_line(caracteres);

            // CHOOSES PREDICTOR:
            if (bp == 0) // Bimodal
            {   
                prediction* pred = bimodal(BHT_bimodal, s, data->instr, data->outcome, res);
                // Print on file
                if (o == 1 && num_branch < 5000)
                {
                    write_line(filename, data->instr, data->outcome, pred->prediction, pred->value, bp, "");
                }
                // Free
                // free_prediction(pred);
            }
            else if (bp == 1) // PShare
            {
                prediction* pred = pshare(BHT_pshare, PHT, s, data->instr, data->outcome, res, ph);
                // Print on file
                if (o == 1 && num_branch < 5000)
                {
                    write_line(filename, data->instr, data->outcome, pred->prediction, pred->value, bp, "");
                }
                // Free
                // free_prediction(pred);
            }
            else if (bp == 2) // GShare
            {
                prediction* pred = gshare(BHT_gshare, reg, s, data->instr, data->outcome, res);
                // Update history register
                reg = history_register(reg, data->outcome, gh);
                // Print on file
                if (o == 1 && num_branch < 5000)
                {
                    write_line(filename, data->instr, data->outcome, pred->prediction, pred->value, bp, "");
                }
                //Free 
                // free_prediction(pred);
            }
            else if (bp == 3) //Tournament
            {
                metaPrediction* pred = tournament(s, data->instr, data->outcome, &reg, ph, gh, res, res_meta, BHT_pshare, BHT_gshare, BHT_meta, PHT);
                // Print on file
                if (o == 1 && num_branch < 5000)
                {
                    write_line(filename, data->instr, data->outcome, pred->prediction, pred->value, bp, pred->predictor);
                }
                // Free
                // free_meta_prediction(pred);
            } 
        }
        
        // Print on screen
        if (bp == 3)
        {
            print_on_screen(res->prediction_type, res_meta->BHT_size, res->g_size, res->p_size, num_branch, res_meta->correct_taken, res_meta->incorrect_taken, res_meta->correct_not_taken, res_meta->incorrect_not_taken);
        }
        else
        {
            print_on_screen(res->prediction_type, res->BHT_size, res->g_size, res->p_size, num_branch, res->correct_taken, res->incorrect_taken, res->correct_not_taken, res->incorrect_not_taken);
        }

        // Free memory space
        free(filename);
        //free_results(res);
        //free_results(res_meta);
        
        //free_BHT(BHT_bimodal);
        //free_BHT(BHT_pshare);
        //free_BHT(BHT_gshare);
        //free_BHT_meta(BHT_meta);
        //free_PHT(PHT);

    }
    fclose(file);
}

dataTrace* parse_line(char* line){
    dataTrace* data = create_data_trace();
    sscanf(line, "%u %s", &data->instr, data->outcome);
    // printf("PC: %u, Outcome: %s\n", data->instr, data->outcome);
    return data;
}

void delete_data_trace(dataTrace* data){
    free(data->outcome);
    free(data);
    // free prediction type
}