# Tarea 1: Predictores de saltos

Foobar is a Python library for dealing with word pluralization.

## Utilización

Para compilar, se utiliza:

```c
make build
```

Para correr el programa, se utiliza:

```c
gunzip -c branch-trace-gcc.trace.gz | branch -s < # > -bp < # > -gh < # > -ph < # > -o < # >
```

## Notas

### Sobre el parámetro s
Para el parámetro s, los predictores de saltos se identifican de la siguiente manera:

0. Bimodal
1. PShare
2. GShare
3. Tournament

### Respecto a la información en consola
La asignación a los determinados espacios se da con el siguiente criterio:

- ```Number of correct prediction of taken branches```: Número de predicciones T que coinciden con el outcome.

- ```Number of incorrect prediction of taken branches```: Número de predicciones T que no coinciden con el outcome.

- ```Correct prediction of not taken branches```: Número de predicciones N que coinciden con el outcome.

- ```Incorrect prediction of not taken branches```: Número de predicciones N que no coinciden con el outcome.

### Doxygen

Para generar la documentación html de Doxygen, ejecute:

```c
make dox
```

Luego, abra el archivo index.html dentro de ./doc/doxy/html.