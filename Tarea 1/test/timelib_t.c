#include "timelib.h"

void delay(int milliseconds)
{
    long pause;
    clock_t now,then;

    pause = milliseconds*(CLOCKS_PER_SEC/1000);
    now = then = clock();
    while( (now-then) < pause )
        now = clock();
}

int main(int argc, char const *argv[])
{
    chronometer* chrono = newChronometer();
    char* path = "./data.txt" ;
    start(chrono);
    delay(5000);
    stop(chrono);

    printf("%f\n",getTime(chrono));   

    writeTime(chrono, path);

    deleteChronometer(chrono);
    return 0;
}
